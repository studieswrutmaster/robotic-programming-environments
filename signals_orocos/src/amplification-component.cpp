#include "amplification-component.hpp"
#include <rtt/Component.hpp>
#include <iostream>

Amplification::Amplification(std::string const& name) : TaskContext(name){
  std::cout << "Amplification constructed !" <<std::endl;

   this->ports()->addEventPort( "inPort", _inPort ).doc( "Input Port that raises an event." );
   this->ports()->addPort( "outPort", _outPort ).doc( "Output Port, here write our data to." );

  addAttribute("k", _k);
}

bool Amplification::configureHook(){
  std::cout << "Amplification configured !" <<std::endl;
  return true;
}

bool Amplification::startHook(){
  std::cout << "Amplification started !" <<std::endl;
  return true;
}

void Amplification::updateHook(){
  double msg;

  if(_inPort.read(msg) == RTT::NewData)
  {
    _outPort.write(msg*_k);
  }
}

void Amplification::stopHook() {
  std::cout << "Amplification executes stopping !" <<std::endl;
}

void Amplification::cleanupHook() {
  std::cout << "Amplification cleaning up !" <<std::endl;
}

/*
 * Using this macro, only one component may live
 * in one library *and* you may *not* link this library
 * with another component library. Use
 * ORO_CREATE_COMPONENT_TYPE()
 * ORO_LIST_COMPONENT_TYPE(Amplification)
 * In case you want to link with another library that
 * already contains components.
 *
 * If you have put your component class
 * in a namespace, don't forget to add it here too:
 */
ORO_CREATE_COMPONENT(Amplification)
