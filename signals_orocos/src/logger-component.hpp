#ifndef OROCOS_SIGNALS_COMPONENT_HPP
#define OROCOS_SIGNALS_COMPONENT_HPP

#include <rtt/RTT.hpp>

#include <std_msgs/Float64.h>

#include <fstream>


class Logger : public RTT::TaskContext{
  public:
    Logger(std::string const& name);
    bool configureHook();
    bool startHook();
    void updateHook();
    void stopHook();
    void cleanupHook();

    RTT::InputPort<double>   _inPort;
    RTT::OutputPort<std_msgs::Float64> _rosPort;


    std::ofstream _myfile;



};
#endif
