#include "generator-component.hpp"
#include <rtt/Component.hpp>
#include <iostream>

Generator::Generator(std::string const& name) : TaskContext(name){
  std::cout << "Generator constructed !" <<std::endl;

   this->ports()->addPort( "outPort", _outPort ).doc( "Output Port, here write our data to." );
  addAttribute("a", _a);
  addAttribute("w", _w);
  addAttribute("t", _t);

  _t = -1;
}

bool Generator::configureHook(){
  std::cout << "Generator configured !" <<std::endl;
  return true;
}

bool Generator::startHook(){
  std::cout << "Generator started !" <<std::endl;
  return true;
}

void Generator::updateHook(){
  _t +=1;
    std_msgs::Float64 msg_send;
    msg_send.data = sin(_t*getPeriod()*_w+_a);
    _outPort.write(msg_send);

}

void Generator::stopHook() {
  std::cout << "Generator executes stopping !" <<std::endl;
}

void Generator::cleanupHook() {
  std::cout << "Generator cleaning up !" <<std::endl;
}

/*
 * Using this macro, only one component may live
 * in one library *and* you may *not* link this library
 * with another component library. Use
 * ORO_CREATE_COMPONENT_TYPE()
 * ORO_LIST_COMPONENT_TYPE(Generator)
 * In case you want to link with another library that
 * already contains components.
 *
 * If you have put your component class
 * in a namespace, don't forget to add it here too:
 */
ORO_CREATE_COMPONENT(Generator)
