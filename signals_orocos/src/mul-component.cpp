#include "mul-component.hpp"
#include <rtt/Component.hpp>
#include <iostream>
#include <cmath>

Mul::Mul(std::string const& name) : TaskContext(name){
  std::cout << "Mul constructed !" <<std::endl;

   this->ports()->addEventPort( "inPort", _inPort ).doc( "Input Port that raises an event." );

   this->ports()->addPort( "outPort", _outPort ).doc( "Output Port, here write our data to." );
}

bool Mul::configureHook(){
  std::cout << "Mul configured !" <<std::endl;
  return true;
}

bool Mul::startHook(){
  std::cout << "Mul started !" <<std::endl;
  return true;
}

void Mul::updateHook(){
  double msg;

  if(_inPort.read(msg) == RTT::NewData)
  {
      /*
    if(acos(msg)-3.14/2>0 && acos(msg)-3.14/2<0)
        msg = -cos(acos(msg)-3.14/2);
    else
        msg = cos(acos(msg)-3.14/2);
*/
    msg = cos(asin(msg)+3.14/2);
    _outPort.write(msg);
  }
}

void Mul::stopHook() {
  std::cout << "Mul executes stopping !" <<std::endl;
}

void Mul::cleanupHook() {
  std::cout << "Mul cleaning up !" <<std::endl;
}

/*
 * Using this macro, only one component may live
 * in one library *and* you may *not* link this library
 * with another component library. Use
 * ORO_CREATE_COMPONENT_TYPE()
 * ORO_LIST_COMPONENT_TYPE(Mul)
 * In case you want to link with another library that
 * already contains components.
 *
 * If you have put your component class
 * in a namespace, don't forget to add it here too:
 */
ORO_CREATE_COMPONENT(Mul)
