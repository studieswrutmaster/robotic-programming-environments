#include "sum-component.hpp"
#include <rtt/Component.hpp>
#include <iostream>

Sum::Sum(std::string const& name) : TaskContext(name){
  std::cout << "Sum constructed !" <<std::endl;

   this->ports()->addEventPort( "inPort1", _inPort1 ).doc( "Input Port that raises an event." );
   this->ports()->addEventPort( "inPort2", _inPort2 ).doc( "Input Port that raises an event." );

   this->ports()->addPort( "outPort", _outPort ).doc( "Output Port, here write our data to." );
}

bool Sum::configureHook(){
  std::cout << "Sum configured !" <<std::endl;
  return true;
}

bool Sum::startHook(){
  std::cout << "Sum started !" <<std::endl;
  return true;
}

void Sum::updateHook(){
  double msg1,msg2;

  if(_inPort1.read(msg1) == RTT::NewData && _inPort2.read(msg2) == RTT::NewData)
  {
    _outPort.write(msg1+msg2);
  }
}

void Sum::stopHook() {
  std::cout << "Sum executes stopping !" <<std::endl;
}

void Sum::cleanupHook() {
  std::cout << "Sum cleaning up !" <<std::endl;
}

/*
 * Using this macro, only one component may live
 * in one library *and* you may *not* link this library
 * with another component library. Use
 * ORO_CREATE_COMPONENT_TYPE()
 * ORO_LIST_COMPONENT_TYPE(Sum)
 * In case you want to link with another library that
 * already contains components.
 *
 * If you have put your component class
 * in a namespace, don't forget to add it here too:
 */
ORO_CREATE_COMPONENT(Sum)
