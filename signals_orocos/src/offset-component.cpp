#include "offset-component.hpp"
#include <rtt/Component.hpp>
#include <iostream>

Offset::Offset(std::string const& name) : TaskContext(name){
  std::cout << "Offset constructed !" <<std::endl;

   this->ports()->addEventPort( "inPort", _inPort ).doc( "Input Port that raises an event." );
   this->ports()->addPort( "outPort", _outPort ).doc( "Output Port, here write our data to." );

  addAttribute("a", _a);
}

bool Offset::configureHook(){
  std::cout << "Offset configured !" <<std::endl;
  return true;
}

bool Offset::startHook(){
  std::cout << "Offset started !" <<std::endl;
  return true;
}

void Offset::updateHook(){
  double msg;

  if(_inPort.read(msg) == RTT::NewData)
  {
    _outPort.write(msg+_a);
  }
}

void Offset::stopHook() {
  std::cout << "Offset executes stopping !" <<std::endl;
}

void Offset::cleanupHook() {
  std::cout << "Offset cleaning up !" <<std::endl;
}

/*
 * Using this macro, only one component may live
 * in one library *and* you may *not* link this library
 * with another component library. Use
 * ORO_CREATE_COMPONENT_TYPE()
 * ORO_LIST_COMPONENT_TYPE(Offset)
 * In case you want to link with another library that
 * already contains components.
 *
 * If you have put your component class
 * in a namespace, don't forget to add it here too:
 */
ORO_CREATE_COMPONENT(Offset)
