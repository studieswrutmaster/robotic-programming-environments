#ifndef OROCOS_SIGNALS_COMPONENT_HPP
#define OROCOS_SIGNALS_COMPONENT_HPP

#include <rtt/RTT.hpp>
#include <std_msgs/Float64.h>

using namespace RTT;

class Generator : public RTT::TaskContext{
  public:
    Generator(std::string const& name);
    bool configureHook();
    bool startHook();
    void updateHook();
    void stopHook();
    void cleanupHook();

    OutputPort<std_msgs::Float64>  _outPort;
    double _a;
    double _w;
    double _t;
};
#endif
