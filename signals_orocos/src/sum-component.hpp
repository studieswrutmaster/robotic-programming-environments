#ifndef OROCOS_SIGNALS_COMPONENT_HPP
#define OROCOS_SIGNALS_COMPONENT_HPP

#include <rtt/RTT.hpp>

class Sum : public RTT::TaskContext{
  public:
    Sum(std::string const& name);
    bool configureHook();
    bool startHook();
    void updateHook();
    void stopHook();
    void cleanupHook();

    RTT::OutputPort<double>  _outPort;
    RTT::InputPort<double>   _inPort1;
    RTT::InputPort<double>   _inPort2;
};
#endif
