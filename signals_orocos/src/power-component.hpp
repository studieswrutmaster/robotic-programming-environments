#ifndef OROCOS_SIGNALS_COMPONENT_HPP
#define OROCOS_SIGNALS_COMPONENT_HPP

#include <rtt/RTT.hpp>

using namespace RTT;


class Power : public RTT::TaskContext{
  public:
    Power(std::string const& name);
    bool configureHook();
    bool startHook();
    void updateHook();
    void stopHook();
    void cleanupHook();

    RTT::OutputPort<double>  _outPort;
    RTT::InputPort<double>   _inPort;


};
#endif
