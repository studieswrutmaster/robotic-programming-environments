#ifndef OROCOS_SIGNALS_COMPONENT_HPP
#define OROCOS_SIGNALS_COMPONENT_HPP

#include <rtt/RTT.hpp>
#include <std_msgs/Float64.h>

class Absolute : public RTT::TaskContext{
  public:
    Absolute(std::string const& name);
    bool configureHook();
    bool startHook();
    void updateHook();
    void stopHook();
    void cleanupHook();

    RTT::OutputPort<std_msgs::Float64>  _outPort;
    RTT::InputPort<std_msgs::Float64>   _inPort;
};
#endif
