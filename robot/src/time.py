#!/usr/bin/env python

import rospy
from std_msgs.msg import Float64

def talker():
    rospy.init_node('time_generator')

    pub = rospy.Publisher('/time_topic', Float64, queue_size=1000)
    rate = rospy.Rate(10) # 10hz

    dtime = 0.1
    time = 0.0


    while not rospy.is_shutdown():

        time += dtime

        msg = Float64()
        msg.data = time

        pub.publish(msg)
        rate.sleep()


if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass