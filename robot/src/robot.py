#!/usr/bin/env python

import rospy

from std_msgs.msg import Float64
from std_msgs.msg import Float64MultiArray

import numpy as np
from sys import path
path.append(r"/home/lukas/casadi-py27-np1.9.1-v3.1.1")
from casadi import *


x = float(rospy.get_param('/robot/init_pose/x',0.0))
rospy.set_param('/robot/init_pose/x',x)

y = float(rospy.get_param('/robot/init_pose/y',0.0))
rospy.set_param('/robot/init_pose/y',y)

theta = float(rospy.get_param('/robot/init_pose/theta',0.0))
rospy.set_param('/robot/init_pose/theta',theta)

initial_pose = [x,y,theta]

def dq(q,u):
	dq = SX.sym("dq",3,1)
	dq[0] = cos(q[2])*u[0]
	dq[1] = sin(q[2])*u[0]
	dq[2] = u[1]
	return dq

def controls_rec(msg):
    global controls
    global timeout_control
    controls = msg.data
    timeout_control = 0


def time_rec(msg):
    global initial_pose
    global system
    global controls
    global timeout_control


    timeout_control += 1
    if( timeout_control >= 2):
        controls = [0.0,0.0]

    time = msg.data

    msg = Float64MultiArray()
    integrator_opts = { 't0':time,
                        'tf':time+0.1}

    Im = integrator("Im", "cvodes", system, integrator_opts)
    sol = Im(x0=initial_pose,p=controls)
    msg.data = np.array(sol["xf"].T).tolist()[0]
    initial_pose = msg.data


    pub.publish(msg)


def node():
    rospy.init_node('robot')

    global pub
    global timeout_control
    global controls
    timeout_control = 0
    controls = [0.0,0.0]

    pub = rospy.Publisher('/pose', Float64MultiArray, queue_size=1000)
    rospy.Subscriber('/time_topic',Float64,time_rec)
    rospy.Subscriber('/controls_topic',Float64MultiArray,controls_rec)
    # s = rospy.Service('add_two_ints', AddTwoInts, set_pose)

    _t = SX.sym("t")
    _q = SX.sym("q",3)
    _u = SX.sym("u",2)

    global system
    system = {'t':_t,
              'x':_q,
              'p':_u,
              'ode':dq(_q,_u)
              }

    rospy.spin()

if __name__ == '__main__':
    try:
        node()
    except rospy.ROSInterruptException:
        pass