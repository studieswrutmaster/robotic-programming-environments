#!/usr/bin/env python

import rospy

from std_msgs.msg import Float64
from std_msgs.msg import Float64MultiArray

import numpy as np

def u1(t):
    # return np.sin(t)
    return 1
def u2(t):
    # return np.cos(t)
    if t > 10.0:
        return 0
    else:
        return 1

def msgReceived(msg):
    # k = float(rospy.get_param('~k'))
    control_msg = Float64MultiArray()
    control_msg.data = [u1(msg.data),u2(msg.data)]
    pub.publish(control_msg)

def node():
    rospy.init_node('control')

    global pub
    pub = rospy.Publisher('/controls_topic', Float64MultiArray, queue_size=1000)
    rospy.Subscriber('/time_topic',Float64,msgReceived)

    rospy.spin()

if __name__ == '__main__':
    try:
        node()
    except rospy.ROSInterruptException:
        pass