#!/usr/bin/env python

import rospy
import matplotlib.pyplot as plt

from std_msgs.msg import Float64MultiArray

def vis(msg):
    global Q1, Q2

    q1 = msg.data[0]
    q2 = msg.data[1]

    Q1.append(q1)
    Q2.append(q2)

def node():
    global Q1,Q2
    Q1 = []
    Q2 = []


    rospy.init_node('tracking_visualization')
    rospy.Subscriber("pose", Float64MultiArray , vis)


    rate = rospy.Rate(5) # 10hz
    plt.figure(1)
    plt.ion()

    while not rospy.is_shutdown():
        ## start tracking the trajectory
        plt.plot(Q1,Q2,'k')
        plt.pause(0.001)
        rate.sleep()

    rospy.spin()


if __name__ == '__main__':
    try:
        node()
    except rospy.ROSInterruptException:
        pass