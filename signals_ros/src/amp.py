#!/usr/bin/env python

import rospy
import numpy as np
import sys


from std_msgs.msg import Float64


def msgReceived(msg):
    # k = float(rospy.get_param('~k'))
    msg.data =  k * msg.data
    pub.publish(msg)

def node():
    rospy.init_node('amp')
    global k

    k = float(rospy.get_param('~k','1.0'))
    rospy.set_param(rospy.resolve_name('~k'),k)


    global pub
    pub = rospy.Publisher('/out', Float64, queue_size=1000)
    rospy.Subscriber('/in',Float64,msgReceived)

    rospy.spin()

if __name__ == '__main__':
    try:
        node()
    except rospy.ROSInterruptException:
        pass
