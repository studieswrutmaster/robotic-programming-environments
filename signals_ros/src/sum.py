#!/usr/bin/env python

import rospy
import numpy as np

from std_msgs.msg import Float64

in1 = 0.0
in2 = 0.0


def msgReceived1(msg):
    global in1
    in1 =  msg.data


def msgReceived2(msg):
    global in2
    in2 =  msg.data

def node():
    rospy.init_node('sum')

    pub = rospy.Publisher('/out', Float64, queue_size=1000)
    rospy.Subscriber('/in1',Float64,msgReceived1)
    rospy.Subscriber('/in2',Float64,msgReceived2)

    rate = rospy.Rate(10) # 10hz

    while not rospy.is_shutdown():


        msg = Float64()
        msg.data = in1 + in2

        pub.publish(msg)
        rate.sleep()

if __name__ == '__main__':
    try:
        node()
    except rospy.ROSInterruptException:
        pass
