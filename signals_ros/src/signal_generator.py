#!/usr/bin/env python

import rospy
import numpy as np

from std_msgs.msg import Float64

def talker():
    rospy.init_node('signal_generator')

    pub = rospy.Publisher('/generator_out', Float64, queue_size=1000)
    rate = rospy.Rate(10) # 10hz

    dtime = 0.1
    time = 0.0

    global k

    w = float(rospy.get_param('~w','1.5707'))
    rospy.set_param(rospy.resolve_name('~w'),w)

    a = float(rospy.get_param('~a','0.0'))
    rospy.set_param(rospy.resolve_name('~a'),a)

    while not rospy.is_shutdown():

        time += dtime

        msg = Float64()
        msg.data = np.sin(w*time+a)
        # msg.data = time

        pub.publish(msg)
        rate.sleep()


if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass


    # from turtlesim.msg import Pose
#
#
# def poseMsgReceived(msg):
#   rospy.loginfo("x: %f",msg.x)
#
# if __name__ == '__main__':
#   rospy.init_node('test_sub_py')
#   rospy.Subscriber('turtle1/pose',Pose,poseMsgReceived)
#   rospy.spin()