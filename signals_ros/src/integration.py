#!/usr/bin/env python

import rospy
import numpy as np

from std_msgs.msg import Float64

# global int_0

init = int(rospy.get_param('/integration/int_0',0.0))
rospy.set_param('/integration/int_0',init)

def msgReceived(msg):
    global init
    msg.data += init
    init = msg.data
    pub.publish(msg)



def node():
    rospy.init_node('integration')

    global pub
    pub = rospy.Publisher('/out', Float64, queue_size=1000)
    rospy.Subscriber('/in',Float64,msgReceived)

    rospy.spin()

if __name__ == '__main__':
    try:
        node()
    except rospy.ROSInterruptException:
        pass
