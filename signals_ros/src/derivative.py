#!/usr/bin/env python

import rospy
import numpy as np

from std_msgs.msg import Float64

global init

init = 0.0

def msgReceived(msg):
    msg.data =  (init - msg.data)
    pub.publish(msg)
    initial = msg.data

def node():
    rospy.init_node('derivative')

    global pub

    pub = rospy.Publisher('/out', Float64, queue_size=1000)
    rospy.Subscriber('/in',Float64,msgReceived)



    rospy.spin()

if __name__ == '__main__':
    try:
        node()
    except rospy.ROSInterruptException:
        pass
